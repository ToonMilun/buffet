/**
 * Called by watch:task-onScormOutput.
 */
module.exports = {
  files: ["<%= outputDir %>\\*.*"]
};
