/**
 * Called by watch:task-onScormOutput.
 */
module.exports = {
  options: {
    tolerance: 1000
  }
};
