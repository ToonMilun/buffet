module.exports = function(grunt){

  // https://www.npmjs.com/package/grunt-prompt

  const path = require('path');
  const chalk = require('chalk');
  const _ = require('underscore');
  const JSON5 = require('json5'); // Out JSON may have /* comments */ in it.
  let Helpers = require('../helpers')(grunt);

  function _chooseTins(value) {
    let tins = [];
    tins = value.split(",");

    let allTins = grunt.config.get("tins");
    let topics = {};

    // For each TIN specified for the user, find the corresponding "topic" definition in the tinsFile.
    tins.forEach(tin => {
      topics[tin] = allTins[tin];
    });

    // Show a list of all topics that were selected. Mark non-existant ones in red.
    let topicStrings = [];
    let topicNotFound = false;
    _.forEach(topics, (val, key) => {
      if (!val) topicNotFound = true;
      topicStrings.push(val ? chalk.bgGreenBright(chalk.black(" " + key + " ")) : chalk.bgRedBright(chalk.black(" " + key + " ")));
    });

    grunt.log.writeln("\n" + topicStrings.join(" "));

    // Generate an on-the-fly buffet file with just the topics that were selected.
    let data = {};
    data.topics = [];
    _.each(topics, (val, key) => {
      if (val) {
        data.topics.push(val);
      }
    });

    grunt.config.set("buffetData", data);

    // If there were missing topics, confirm it with the user.
    if (topicNotFound) {
      grunt.task.run("prompt:confirmMissingTins");
    }
  }

  return {

    // There's getting to be a lot of *.buffet files. This will help organize them.
    chooseMode: {
      options: {
        questions: [
          {
            config: 'chooseMode',   // arbitrary name or config for any other grunt task
            type: 'list',           // list, checkbox, confirm, input, password
            choices: function() {

              // Returns parts of the path to be expanded by the grunt.file.expand() call.
              return [
                {name: "Recent",        value: "recent"},
                {name: "BU**",          value: "BU"},
                {name: "IT**",          value: "IT"},
                {name: "KE**",          value: "KE"},
                {name: "SIT**",         value: "SIT"},
                {name: "SHOW**",        value: "SHOW"},
                {name: "Short courses", value: "short-courses/"},
                {name: "Custom (TINs)", value: "tin"}
              ]


            },
            message: 'Choose a content type: ',  // Question to ask the user
            validate: function(value){   // return true if valid, error message if invalid. works only with type:input
              return true;
            }
          }
        ],
        then: function(results, done) {
    
          let choice = results.chooseMode;
          switch (choice) {

            // Show recent selections.
            case "recent":
              grunt.task.run("prompt:chooseRecent");
              return;

            // Custom (TINs) gets a different implementation.
            case "tin":
              grunt.task.run("prompt:chooseTins");
              return;

            default:
              grunt.config.set("modeFilter", choice);
              grunt.task.run(`prompt:buffetFilename`);
              break;
          }
        }
      }
    },


    // Show recent selections.
    chooseRecent: {
      options: {
        questions: [
          {
            config: 'chooseRecent',   // arbitrary name or config for any other grunt task
            type: 'list',             // list, checkbox, confirm, input, password
            choices: function() {

              let saveData = Helpers.getSaveData();
              let recent = saveData.recent?.length ? 
                saveData.recent.map(e => {return {name: e, value: e};}) : 
                [{name: "NO RECENT", value: 0}];
              return recent;
            },
            message: 'Choose a recent selection: ',  // Question to ask the user
            validate: function(value){   // return true if valid, error message if invalid. works only with type:input
              return true;
            }
          }
        ],
        then: function(results, done) {
          
          grunt.log.ok(JSON.stringify(results));

          let choice = results.chooseRecent;
          switch (choice) {

            // Show recent selections.
            case 0:
              grunt.fail.fatal("No recent data to choose from. Make some choices normally, then try again.");
              return;

            default:

              let fileType = choice.indexOf(".buffet") >= 0 ? "file" : "tin"
              if (fileType === "tin") {
                _chooseTins(choice);
              }
              else {
                grunt.config.set("buffetFilename", choice);
              }
              break;
          }
        }
      }
    },
    

    // Allow the user to input specific TIN(s) for processing.
    chooseTins: {
      options: {
        questions: [
          {
            config: 'chooseTins',   // arbitrary name or config for any other grunt task
            type: 'input',           // list, checkbox, confirm, input, password
            message: 'Which TINs?: ',  // Question to ask the user
            filter:  function(value){   // modify the answer
              
              const TIN_LENGTH = grunt.config.get("tinLength");

              // Trim any whitespace around the TIN numbers provided.
              let tins = [];
              tins = value.split(",").map(e => {
                
                let tin = e.trim();

                // Pad with leading zeroes.
                // Example:
                // '12' -> '00012'
                while (tin.length < TIN_LENGTH) tin = "0" + tin;

                return tin;
              });

              // Ensure that duplicates weren't added.
              tins = _.unique(tins);

              // Sort in order.
              tins = tins.sort();
              
              return tins.join(",");
            },
            validate: function(value){   // return true if valid, error message if invalid. works only with type:input
              if (!value.length) return chalk.redBright("Please enter one or more TIN numbers, seperated by commas.\nExample: '1,2,28'");
              return true;
            }
          }
        ],
        then: function(results, done) {

          // Check which of the TINs selected exist, then
          // display each TIN selected in order.
          let value = results.chooseTins;
          return _chooseTins(value);
        }
      }
    },


    // Ask the user to input specific files for processing.
    chooseMacro: {
      options: {
        questions: [
          {
            config: 'chooseMacro',    // arbitrary name or config for any other grunt task
            type: 'input',            // list, checkbox, confirm, input, password
            message: chalk.greenBright('Which *.buffet files? (Wildcards permitted): '),  // Question to ask the user
            filter:  function(value){   // modify the answer
              
              const TIN_LENGTH = grunt.config.get("tinLength");

              // Trim any whitespace around the filenames provided.
              let items = [];
              items = value.split(",").map(e => {
                return e.trim();
              });

              // Ensure that duplicates weren't added.
              items = _.unique(items);
              
              return items.join(",");
            },
            validate: function(value){   // return true if valid, error message if invalid. works only with type:input
              if (!value.length) return chalk.redBright(`Please enter one or more *.buffet names, seperated by commas. Example:
              
SIT01,SIT5*,BUS**`);
              return true;
            }
          }
        ],
        then: function(results, done) {

          // Find all the files that this selection would yield.
          const buffetDir = grunt.config.get("buffetDir");
          const buffetExt = grunt.config.get("buffetExt");

          let items = [];

          grunt.file.expand({filter: 'isFile', cwd: buffetDir}, results.chooseMacro.split(",").map(e => {return e + "." + buffetExt})).forEach((p) => {

            // Push the filename (without extension) to the items.
            items.push(path.basename(p, "." + buffetExt));
          });

          // If no valid items found; show an eror.
          if (!items.length) {
            grunt.fail.fatal(`Could not find any *.buffet files with this selection. Make sure you're using the following format:
            
SIT01,SIT5*,BUS**`);
            return;
          }

          // Otherwise, list them.
          items = items.sort();
          grunt.log.writeln(chalk.bgGreenBright(chalk.black(`
Units found:`)));
          grunt.log.writeln(chalk.greenBright(items.join(", ")));

          grunt.config.set("macroUnits", items);
        }
      }
    },
    chooseTaskMacro: {
      options: {
        questions: [
          {
            config: 'chooseTaskMacro',
            type: 'list',
            choices: function() {
              return [
                {
                  name: "Setup/sync all topics",
                  value: "setup"
                },
                {
                  name: "Setup + SCORM each individual topic",
                  value: "scorm"
                },
                {
                  name: "Setup + build and upload all topics",
                  value: "upload"
                },
                {
                  name: "Commit and push all Adapt course repos",
                  value: "pushAdapt"
                }
              ];
            },
            message: function() {
              return chalk.greenBright('What would you like to do to these units?: ');
            }
          }
        ],
        // Store the user's response.
        then: function(results, done) {
          grunt.config.set("macroChooseTask", results.chooseTaskMacro);
        }
      }
    },
    confirmMacro: {
      options: {
        questions: [
          {
            config: 'confirmMacro',   // arbitrary name or config for any other grunt task
            type: 'confirm',               // list, checkbox, confirm, input, password
            message: function() {

              grunt.log.writeln(chalk.bgRedBright(chalk.black("Are you sure?")) + " This task will run for a long time, and process a large amount of files. It is recommended that you not touch your computer while it is running. Confirming this prompt will start the process.");

              return "Proceed?";
            }
          }
        ],
        // If the user answered false, throw an error (getting them to re-run the program once they're ready).
        then: function(results, done) {
          if (!results.confirmMacro) grunt.fail.fatal("Terminating program.");
        }
      }
    },










    confirmMissingTins: {
      options: {
        questions: [
          {
            config: 'confirmMissingTins',   // arbitrary name or config for any other grunt task
            type: 'confirm',               // list, checkbox, confirm, input, password
            message: function() {

              grunt.log.writeln(chalk.bgRedBright(chalk.black("One or more TIN numbers have not been defined in 'tins.buffet'. They will be skipped!")) + "\n");

              return "Proceed?";
            }
          }
        ],
        // If the user answered false, throw an error (getting them to re-run the program once they're ready).
        then: function(results, done) {
          if (!results.confirmMissingTins) grunt.fail.fatal("Please check your 'tins.buffet' file, and try again.");
        }
      }
    },

    // Buffet selection.
    buffetFilename: {
      options: {
        questions: [
          {
            config: 'buffetFilename',   // arbitrary name or config for any other grunt task
            type: 'list',               // list, checkbox, confirm, input, password
            choices: function() {
              
              const buffetDir = grunt.config.get("buffetDir");
	            const buffetExt = grunt.config.get("buffetExt");
              let files = [];
              
              const filter = grunt.config.get("modeFilter");

              grunt.file.expand({filter: 'isFile', "cwd": buffetDir}, path.join(`${filter}**.${buffetExt}`)).forEach(function(p) {
                
                // Only add the buffet files which match the filter.
                /*let r = new RegExp("^" + filter, "i");
                if (!path.basename(p).match(r)) return;*/

                files.push({
                  name:   path.basename(p, "." + buffetExt),
                  value:  p
                });
              });
              if (files) files[0].checked = true;

              return files;
            },
            message: 'Create which content?: ',  // Question to ask the user
            validate: function(value){  // return true if valid, error message if invalid. works only with type:input
              return true;
            }, 
            filter:  function(value){   // modify the answer

              /*if ((value + "").length < 10) {  // ISBN is too short.
                //const chalk = require('chalk');
                //grunt.log.writeln(chalk.bgRed("ERROR: ISBN provided is too short. Using a fallback value."));
                return "9780000000000";
              } */
              
              return value;
            }
          }
        ]
      }
    },
    confirmAdaptFwks: {
      options: {
        questions: [
          {
            config: 'confirmAdaptFwks',   // arbitrary name or config for any other grunt task
            type: 'confirm',               // list, checkbox, confirm, input, password
            message: function() {

              Helpers.logBanner('This build uses the following Adapt frameworks (fwk):');

              grunt.config("buildData").adaptFwks.forEach(f => {
                grunt.log.writeln(chalk.yellowBright(f));
              });

              grunt.log.writeln("\n" + chalk.bgRedBright(chalk.black('Confirm that all Adapt frameworks (sources) being used are up-to-date for their respective topics.')));
              grunt.log.writeln("\n" + chalk.bgRedBright(chalk.black('Confirm that you understand that this program will clone each Adapt Topic\'s "src" and "fwk" to it\'s "git" directory on your LOCAL machine (' + grunt.config.get("localAdaptRepoDir") + ').')) + "\n");

              return "Proceed?";
            },
            // Only ask this question if any Adapt units are being used.
            when: function() {

              let buildData = grunt.config.get("buildData");

              // Find and store an array of all framework SRCs (these will be used by the "prompt:confirmAdaptFwks" task).
              let fwks = [];
              grunt.config.get("topics").forEach(t => {
                if (t.fwk) fwks.push(t.fwk);
              });
              fwks = _.uniq(fwks);
              buildData.adaptFwks = fwks;
              grunt.config.set("buildData", buildData);

              return Boolean(fwks);
            }
          }
        ],
        // If the user answered false, throw an error (getting them to re-run the program once they're ready).
        then: function(results, done) {
          if (!results.confirmAdaptFwks) grunt.fail.fatal("Please check and update your Adapt framework(s), and try again.");
        }
      }
    },
    /**
     * Ask the user to choose which topic is processed (or "all"|"edge"|"adapt" to process only those).
     */
    chooseTopic: {
      options: {
        questions: [
          {
            config: 'chooseTopic',      // arbitrary name or config for any other grunt task
            type: 'list',               // list, checkbox, confirm, input, password
            choices: function() {
              
              const topics = grunt.config.get("topics");
              
              let choices = [
                {
                  name: "[ALL]",
                  value: "all"
                },
                {
                  name: "[ADAPT ONLY]",
                  value: "adapt"
                },
                {
                  name: "[EDGE ONLY]",
                  value: "edge"
                }
              ];
              topics.forEach(topic => {
                choices.push({
                  name: "Topic " + topic.id,
                  value: topic.id
                });
              });

              return choices;
            },
            message: 'Process which topic(s)?: '  // Question to ask the user
          }
        ],
        // Run the next task based on the user's response.
        then: function(results, done) {

          let topics = grunt.config.get("topics");

          // Remove specific elements from the config.topics array depending on the choice.
          // (The tasks following this one will process ALL elements in the config.topics, so to remove an element is to prevent it
          // being processed).
          let choice = results.chooseTopic;
          switch (choice) {

            case "all":
              // Do nothing. The "topics" array is already full of all available topics.
              break;

            // Process only the Adapt topics.
            case "adapt":
              grunt.config.set("topics", _.filter(topics, (t) => {return t.type == "adapt";}));
              break;

            // Process only the Edge topics.
            case "edge":
              grunt.config.set("topics", _.filter(topics, (t) => {return t.type == "edge";}));
              break;

            // Process only a specific topic.
            default:
              grunt.config.set("topics", [_.find(topics, (t) => {return t.id == choice;})]);
              break;
          }

          grunt.task.registerTask("__promptMsg", "", () => {
            let t = [];
            grunt.config.get("topics").forEach((topic) => {t.push(topic.id);});
            grunt.log.writeln("Topics to be processed: " + chalk.yellowBright(t.join(", ")));
          });
          grunt.task.run("__promptMsg");
        }
      }
    },
    chooseTask: {
      options: {
        questions: [
          {
            config: 'chooseTask',
            type: 'list',
            choices: function() {
              return [
                {
                  name: "Setup/sync all topics",
                  value: "setup"
                },
                {
                  name: "Setup + SCORM each individual topic",
                  value: "scorm"
                },
                /*{
                  name: "SCORM each individual topic (skip setup & pull)",
                  value: "scormFast"
                },*/
                {
                  name: "Setup + build and upload all topics",
                  value: "upload"
                },
                {
                  name: "Setup + SCORM each individual topic " + chalk.redBright("(AAs only!)"),
                  value: "scormAA"
                },
                {
                  name: "Setup + dev and upload all topics " + chalk.redBright("(AAs only!)"),
                  value: "uploadAA"
                },
                {
                  name: "EPUB these topics",
                  value: "epub"
                },
                {
                  name: "Commit and push all Adapt course repos",
                  value: "pushAdapt"
                },
                {
                  name: "TBA: More features.",
                  value: "tba"
                }
              ];
            },
            message: function() {
              return 'What would you like to do?: ';
            }
          }
        ],
        // Run the next task based on the user's response.
        then: function(results, done) {

          let topics = grunt.config.get("topics");

          switch (results.chooseTask) {
            case "setup":
              Helpers.logBanner("Setting up all topics");
              grunt.task.run(`task-setupTopic`);
              break;
              
            case "pushAdapt":
              Helpers.logBanner("Committing, pulling, then pushing all Adapt COURSE repos");
              grunt.task.run(`task-pushAdaptTopic`);
              break;

            case "scormFast":

              // Clean the output folder.
              grunt.task.run("task-clean");

              // Log this AFTER all the "prepare" tasks above have completed.
              grunt.task.registerTask("__promptMsg", "", () => {Helpers.logBanner("SCORMing all topics");});
              grunt.task.run("__promptMsg");
              
              grunt.config.set("adaptBuildMode", "scorm"); // FIXME: Rushed implementation.
              grunt.task.run(`task-scormTopic`);

              break;

            case "scorm":
            case "scormAA":
              // Clean the output folder.
              grunt.task.run("task-clean");
            
            case "scorm": 
            case "scormAA": 
            case "upload":
            case "uploadAA":
              
              // Set up all topics first
              // (The actual SCORMing step take a lot longer than setting them up, so it's better to perform the setup for all
              // topics first, and then the SCORMing for all topics).
              Helpers.logBanner("Preparing all topics for SCORM");
              //topics.forEach((t, i) => {grunt.task.run(`task-setupTopic:${i}`);});

              grunt.task.run(`task-setupTopic`);

              // Log this AFTER all the "prepare" tasks above have completed.
              grunt.task.registerTask("__promptMsg", "", () => {Helpers.logBanner("SCORMing all topics");});
              grunt.task.run("__promptMsg");
              
              grunt.config.set("adaptBuildMode", results.chooseTask); // FIXME: Rushed implementation.
              grunt.task.run(`task-scormTopic`);

              break;
            
            case "epub":

                // Clean the output folder.
                grunt.task.run("task-clean");
              
                // Set up all topics first
                // (The actual SCORMing step take a lot longer than setting them up, so it's better to perform the setup for all
                // topics first, and then the SCORMing for all topics).
                Helpers.logBanner("Preparing all topics for EPUB");
                //topics.forEach((t, i) => {grunt.task.run(`task-setupTopic:${i}`);});
  
                grunt.task.run(`task-setupTopic`);
  
                // Log this AFTER all the "prepare" tasks above have completed.
                grunt.task.registerTask("__promptMsg", "", () => {Helpers.logBanner("EPUBing all topics");});
                grunt.task.run("__promptMsg");
                
                grunt.config.set("adaptBuildMode", "epub"); // FIXME: Rushed implementation.
                grunt.task.run(`task-scormTopic`);
  
                break;

            default:
              grunt.fail.fatal("Sorry, that option isn't implemented yet.");
          }
        }
      }
    }
  }
};
