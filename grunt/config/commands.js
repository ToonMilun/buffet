/**
 * Execute CMD commands.
 */
module.exports = {

  /**
   * FIXME: only one command type can be run per task (due to the commands need to use grunt.config values).
   */

  "zip-edge": {
    // Run a custom Batch script to zip the contents and rename the modifier files within.
    cmd: '<%= batchDir %>\\zip-edge.bat <%= _zipEdgeArgs %>'
  },
  // FIXME: Is this used for anything?
  "npm-install-adapt": {
    // Installs Node modules for the provided directory.
    cmd: '<%= batchDir %>\\npm-install-adapt.bat <%= _npmInstallAdaptArgs %>'
  },
  "build-adapt": {
    // Runs "grunt build --modifier(s)" for the provided directory.
    cmd: '<%= batchDir %>\\build-adapt.bat <%= _buildAdaptArgs %>'
  },
  "custom": {
    // Runs any custom CMD command string passed as argument0.
    cmd: '<%= _customArgs %> & exit /b 0'
  },
  "test": {
    // Runs any custom CMD command string passed as argument0.
    cmd: 'start echo wubba & pause & exit /b 0'
  }
}; //https://github.com/luozhihua/grunt-commands
