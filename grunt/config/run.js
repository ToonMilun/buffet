/**
 * Execute CMD commands.
 * https://www.npmjs.com/package/grunt-run
 */
module.exports = {

  /**
   * IMPORTANT!!!
   * ------------
   * - The order of the "passArgs" property below is the order that the batch script is passed the parameters.
   *   Example: "git-clone" below will create the batch command:
   *   popup.bat git-clone.bat ${repo} ${dir}
   * 
   * - If you need to pass an array (like "mods") to the batch script, it MUST be the LAST parameter, and
   *   the array must be joined with "#".
   *   Example: "git-commit-push" below will create the batch command:
   *   popup.bat git-commit-push.bat ${dir} ${mods[0]}#${mods[1]}#${mods[2]}
   * 
   *   A valid "mods" value would be:
   *   el0#el1#el2#el3
   * 
   *   The popup.bat will then convert this to:
   *   el0 el1 el2 el3
   * 
   *   And popup.bat will run:
   *   git-commit-push.bat ${dir} el0 el1 el2 el3
   * 
   *   Basically making each element of the array into its own parameter (batch does not natively support arrays; this is the workaround).
   */

  "git-clone": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "git-clone.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "repo",
        "dir"
      ]
    }
  },

  "git-fix-repo": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "git-fix-repo.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "repo"
      ]
    }
  },

  "git-pull": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "git-pull.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "dir"
      ]
    }
  },

  "git-commit-pull-push": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "git-commit-pull-push.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "dir",
        "mods"
      ]
    }
  },

  "npm-install": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "npm-install.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "dir"
      ]
    }
  },

  "build-adapt": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "build-adapt.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "dir",
        "output",
        "mods"
      ]
    }
  },

  "build-scorm-adapt": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "build-scorm-adapt.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "dir",
        "output",
        "mods"
      ]
    }
  },

  "build-upload-adapt": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "build-upload-adapt.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "dir",
        "output",
        "mods"
      ]
    }
  },

  "dev-upload-adapt": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "dev-upload-adapt.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "dir",
        "output",
        "mods"
      ]
    }
  },

  "epub-adapt": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "epub-adapt.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "dir",
        "output",
        "mods"
      ]
    }
  },

  "zip-edge": {
    cmd: '<%= batchDir %>\\popup.bat',
    args: [
      "zip-edge.bat" // The name of the *.bat to open in the popup.
    ],
    options: {
      ready: 100,
      failOnError: true,
      passArgs: [
        "dir",
        "output",
        "num",
        "mods"
      ]
    }
  }
};
