const chalk = require('chalk');
const _ = require('lodash');

module.exports = function(grunt) {

  let exports = {

    // Logs a decorative banner to denote the start of a new major process.
    logBanner: function(bannerStr) {

      grunt.log.writeln("");
      grunt.log.writeln(chalk.bgMagentaBright("".padStart(bannerStr.length, " ")));
      grunt.log.writeln("");
      grunt.log.writeln(chalk.magentaBright(bannerStr));
      grunt.log.writeln("");
      grunt.log.writeln(chalk.bgMagentaBright("".padStart(bannerStr.length, " ")));
      grunt.log.writeln("");
    },

    // Creates the SAVFILE if none exists.
    // Returns save data.
    getSaveData: function() {

      const SAVFILE = grunt.config.get("savFile");

      if (grunt.file.exists(SAVFILE)) return grunt.file.readJSON(SAVFILE);

      grunt.file.write(SAVFILE, JSON.stringify({
        recent: []
      }));
      grunt.file.readJSON(filepath)
    },

    setSaveData: function(saveData) {

      const SAVFILE = grunt.config.get("savFile");

      grunt.file.write(SAVFILE, JSON.stringify(saveData));
    },

    // Stores the provided "str" into the .recent file.
    // This is used to retrieve the most recent Buffet selection processed.
    saveRecent: function(val) {

      let saveData = this.getSaveData();

      // Prepend the value to the start of the recent array.
      saveData.recent = [val].concat(saveData.recent);

      // Ensure there are no doubles.
      saveData.recent = _.uniq(saveData.recent);

      // Remove any blank entries.
      saveData.recent = _.filter(saveData.recent, e => {return Boolean(e);});

      this.setSaveData(saveData);
    }


  };

  return exports;
};
