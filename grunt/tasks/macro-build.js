module.exports = function(grunt) {
	grunt.registerTask('macro-build', 'Performs the necessary setup before performing the same set of Buffet tasks on multiple units. Logs to output.', function(mode) {

		const chalk = require('chalk');
		const path = require('path');
		const _ = require('underscore');

		const outputDir = grunt.config.get("outputDir");

		// Passed in by prompt.
		const units = grunt.config.get("macroUnits");
		const buildTask = grunt.config.get("macroChooseTask");

		const FINALLOG = path.join(outputDir, `__summary__.log`);
		let finalLog = []; // Array of shorthand info on each unit.

		// Run a seperate 'grunt auto' task for each unit.
		units.forEach((unit, idx) => {

			const taskName = `build-${idx}--${unit}`;
			grunt.task.registerTask(taskName, "", function() {
				
				let done = this.async();

				grunt.util.spawn({
					cmd: 'grunt',
					args: ["auto", `--unit=${unit}`, `--tsk=${buildTask}`]
				}, (error, result, code) => {
					
					if (error) {
						grunt.log.writeln(chalk.bgRedBright(chalk.black("ERROR!")) + " " + chalk.redBright(`Check ${unit}.log`));
						finalLog.push(`${unit} ---------- ERROR ---------- ${new Date().toString()}`);
					}
					else {
						grunt.log.writeln(chalk.bgGreenBright(chalk.black("SUCCESS!")));
						finalLog.push(`${unit} ---------- SUCCESS -------- ${new Date().toString()}`);
					}
		
					// Write the results for this unit to their own *.log file.
					// Give a date header.
					const LOGFILE = path.join(outputDir, `${unit}.log`);
					grunt.file.write(LOGFILE, `${error ? "<ERROR>" : "<SUCCESS>"}
${new Date().toString()}

${result.stdout}`);
		
					// Write to the final log. Deliberately written after each iteration (in case an unexpected crash happens).
					grunt.file.write(FINALLOG, finalLog.join("\n"));

					done();
				});
			});
        	grunt.task.run(taskName);
		});
	});
};