module.exports = function(grunt) {
	grunt.registerTask('intro', 'Print intro messages', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');

		let art = [
			[0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,1,2,1,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,1,2,2,1],
			[0,0,0,0,1,1,1,0,0,1,1,1,2,1,1,1],
			[0,0,0,1,4,4,4,1,1,4,3,1,1,1,0,0],
			[0,0,1,4,4,4,3,4,3,3,3,3,1,0,0,0],
			[0,0,1,4,3,3,3,3,3,3,3,3,1,1,0,0],
			[0,1,4,3,3,3,3,3,3,3,3,3,1,2,1,0],
			[0,1,4,3,3,3,3,3,3,3,3,1,1,1,2,1],
			[1,2,1,1,3,3,3,3,3,3,1,1,1,1,2,1],
			[1,2,2,1,1,1,1,1,1,1,1,1,2,2,1,1],
			[0,1,1,2,2,2,2,2,2,2,2,2,1,1,1,1],
			[0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0]
		]
		let palette = [
			"bgBlack",
			"bgBlack",
			"bgWhite",
			"bgRed",
			"bgRedBright"
		]

		grunt.log.writeln(chalk.bgRedBright("\n         ") + " " + chalk.redBright("DD Buffet") + " " + chalk.bgRedBright("          "));
		
		art.forEach(row => {
			row.forEach(px => {
				grunt.log.write(chalk[palette[px]]("  "));
			});
			grunt.log.write("\n");
		});

		grunt.log.writeln(chalk.bgYellowBright(chalk.black("\nIMPORTANT!")) + " " + chalk.yellowBright("You need GIT installed: https://git-scm.com/download/win"));

		grunt.log.writeln(chalk.bgYellowBright(chalk.black("\nIMPORTANT!")) + " " + chalk.yellowBright("This program runs better from the office. Going through the VPN will make it very slow!"));

		grunt.log.writeln(chalk.bgYellowBright(chalk.black("\nWARNING!")) + " " + chalk.yellowBright("This program will access the Didasko R: drive, and will process a large quantity of files from it. If you are unsure of what you are doing, please close this program IMMEDIATELY!\n- Milton Plotkin"));
	});
};