module.exports = function(grunt) {
	grunt.registerTask('macro', 'Performs the necessary setup before performing the same set of Buffet tasks on multiple units. Logs to output.', function(mode) {

		const chalk = require('chalk');
		const path = require('path');
		const _ = require('underscore');

		let art = [
			[0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,1,2,1,0],
			[0,0,0,0,0,0,0,0,0,0,0,0,1,2,2,1],
			[0,0,0,0,1,1,1,0,0,1,1,1,2,1,1,1],
			[0,0,0,1,4,4,4,1,1,4,3,1,1,1,0,0],
			[0,0,1,4,4,4,3,4,3,3,3,3,1,0,0,0],
			[0,0,1,4,3,3,3,3,3,3,3,3,1,1,0,0],
			[0,1,4,3,3,3,3,3,3,3,3,3,1,2,1,0],
			[0,1,4,3,3,3,3,3,3,3,3,1,1,1,2,1],
			[1,2,1,1,3,3,3,3,3,3,1,1,1,1,2,1],
			[1,2,2,1,1,1,1,1,1,1,1,1,2,2,1,1],
			[0,1,1,2,2,2,2,2,2,2,2,2,1,1,1,1],
			[0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0]
		]
		let palette = [
			"bgBlack",
			"bgBlack",
			"bgWhite",
			"bgGreen",
			"bgGreenBright"
		]

		grunt.log.writeln(chalk.bgGreenBright("\n         ") + " " + chalk.greenBright("DD Buffet") + " " + chalk.bgGreenBright("          "));
		
		art.forEach(row => {
			row.forEach(px => {
				grunt.log.write(chalk[palette[px]]("  "));
			});
			grunt.log.write("\n");
		});

		grunt.log.writeln(chalk.bgYellowBright(chalk.black("\nIMPORTANT!")) + " " + chalk.yellowBright("You need GIT installed: https://git-scm.com/download/win"));

		grunt.log.writeln(chalk.bgYellowBright(chalk.black("\nWARNING!")) + " " + chalk.yellowBright("You are about to run multiple sequential Buffet tasks. This feature is still in early development. Try not to interrupt this process."));
		
		grunt.log.writeln(chalk.bgRedBright(chalk.black("\nWARNING!")) + " " + chalk.redBright("Do NOT use this to process assessment/activities units! It hasn't been implemented yet."));

		grunt.log.writeln(chalk.yellowBright("\n----------------------------------------------------------------\n"));


		// Clean the output folder.
		grunt.task.run("task-clean");
		grunt.task.run("prompt:chooseMacro");
		grunt.task.run("prompt:chooseTaskMacro");
		grunt.task.run("prompt:confirmMacro");
		grunt.task.run("macro-build");

	});
};