module.exports = function(grunt) {
	grunt.registerTask('default', [
		'intro',					// Show the intro splashscreen.
		
		'build-loadTins',			// Load in the base TIN data from 'tins.buffet'.
		
		"prompt:chooseMode", 		// Get the user to choose a category.
		"build-setupData", 			// Check that the data in the provided *.buffet file is valid.

		// TEMP.
		//"task-scorm"

		"prompt:confirmAdaptFwks", 	// Allows the user a chance to exit the program if any Adapt source is out of date.

		"prompt:chooseTopic",		// Give the user the option to process only specific topics.
		
		"prompt:chooseTask"			// Ask the user to select which operation to perform on the valid topics.
									// This task will initiate the next task based on the option chosen.*/
	]);
};