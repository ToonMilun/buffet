module.exports = function(grunt) {
	grunt.registerTask('auto-setup', 'Performs the necessary setup prior to automating a large amount of buffet tasks.', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');
		const _ = require('underscore');
		
		// FIXME: These are directly copied from "prompt:chooseTask".
		// I couldn't find a way to get that task to skip forcing the user to manually select its options.
		const tasks = {
			"setup": [
				"task-setupTopic"
			],
			"pushAdapt": [
				"task-pushAdaptTopic"
			],
			"scorm": [
				"task-setupTopic",
				"task-scormTopic"
			]
		}


		// Determine which unit is about to be processed
		// ---------------------------------------------
		const unit = grunt.option('unit');
		if (!unit) {
			grunt.fail.fatal(`No "unit" parameter found. Example:

grunt auto --unit=SIT01 --tsk=scorm`);
			return;
		}


		// Determine which task that needs to be performed on said unit
		// ------------------------------------------------------------
		const task = grunt.option('tsk'); // Can't call it "task" (reserved name).
		grunt.log.ok(process.argv);
		if (!task) {
			grunt.fail.fatal(`No "tsk" parameter found. Example:

grunt auto --unit=SIT01 --tsk=scorm`);
			return;
		}

		const taskList = tasks[task];
		if (!taskList) {
			grunt.log.error(`Invalid "task" value. Valid options are:`);
			_.each(tasks, (val, key) => {
				grunt.log.error(`- ${key}`);
			});
			grunt.fail.fatal("Fix above error and try again.");
			return;
		}
		

		// Convert from SIT01 -> SIT01.buffet.
		const buffetFilename = unit.match(/.buffet$/) ? unit : unit + ".buffet";
		
		// Set the variable that build-setupData will read.
		grunt.config.set("buffetFilename", buffetFilename);

		// Check that the data in the provided *.buffet file is valid.
		grunt.task.run("build-setupData");

		// Run all the tasks.
		taskList.forEach(e => {
			grunt.task.run(e);
		});
	});
};