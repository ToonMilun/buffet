module.exports = function(grunt) {
	grunt.registerTask('build-loadTins', 'Loads the data from "tins.buffet" into grunt.config.', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');
		const _ = require('underscore');
		const JSON5 = require('json5'); // Out JSON may have /* comments */ in it.
		let Helpers = require('../helpers')(grunt);


		const tinsFile = grunt.config.get("tinsFile");
        let data = JSON5.parse(grunt.file.read(tinsFile));
		let tins = {}; 

		let topicSettings = data.topicSettings;
		data.topics.forEach(topic => {

			if (!topic.tin) 
				grunt.fail.fatal("No 'tin' property defined for a topic in 'tins.buffet'\n\n" + JSON.stringify(topic, "", 4));

			// Have each tin be an extend of the topicSettings from the 'tins.buffet' file.
			tins[topic.tin] = _.extend({}, topicSettings, topic);
		});

		grunt.config.set("tins", tins);
	});
};