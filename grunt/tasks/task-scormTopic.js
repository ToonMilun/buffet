module.exports = function(grunt) {
	grunt.registerTask('task-scormTopic', '(Builds and) SCORMs all topics and places their .zip into the outputDir.', function(topicIdx) {
		
		const path = require('path');
		const chalk = require('chalk');
		const _ = require('underscore');
		let Helpers = require('../helpers')(grunt);
		
		let done = this.async();
		let topics = grunt.config.get("topics");

		const outputDir = grunt.config.get("outputDir");
		let topicsComplete = 0;

		// Wait for all topics to be ready before ending this task.
		let onComplete = () => {
			topicsComplete++;
			if (topicsComplete == topics.length) {

				Helpers.logBanner("SCORM results");

				// Show a summary of the outputDir.
				// FIXME: Sometimes throws errors. Added a lazy try catch for now.
				try {
					grunt.file.expand({filter: 'isFile', "cwd": outputDir}, ["**.zip", "**.log"]).forEach((p) => {

						let zipName = path.basename(p).split(".")[0];
						let topic = _.find(topics, t => {
							if (t.tin) return t.tin.toString() == zipName;
							return t.num.toString() == zipName; 
						});

						topic.resetHeader();
						if (path.extname(p) == ".zip") {
							topic.logHeader(chalk.greenBright(p));
						}
						else {
							topic.logHeader(chalk.redBright(p + " (ERROR: See CMD window!)"));
						}

						topic.logHeader(chalk.yellowBright("TODO: Generate imsmanifest for Edge topics."));
					});
				}
				catch (err) {
					// 
				}

				done();
			}
		};

		// Process all at once.
		topics.forEach((t, i) => {
			t.scorm(grunt, onComplete);
		});

		return;
	});
};