module.exports = function(grunt) {
	grunt.registerTask('task-clean', 'General cleanup at the start of the tasks', function(mode) {
		
		const path = require("path");

		// Delete any batch script generated files that were created (*.log & *.zip)
		grunt.file.expand({filter: 'isFile'}, [
			path.join(grunt.config.get("outputDir"), "*.log"),
			path.join(grunt.config.get("outputDir"), "*.zip")
		]).forEach(function(p) {
			grunt.file.delete(p);
		});
	});
};