module.exports = function(grunt) {
	grunt.registerTask('exit', 'Exits out of the grunt process (use this to stop a "watch" task).', function(mode) {
		
		process.exit(1);
	});
};