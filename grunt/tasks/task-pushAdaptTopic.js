module.exports = function(grunt) {
	grunt.registerTask('task-pushAdaptTopic', 'Commits and pushes all changes across the selected Adapt Topics.', function() {
		
		const _ = require("underscore");

		let done = this.async();
		let adaptTopics = _.filter(grunt.config.get("topics"), t => {
			return t.type == "adapt";
		});

		let topicsComplete = 0;

		// Wait for all topics to be ready before ending this task.
		let onComplete = () => {
			topicsComplete++;
			if (topicsComplete == adaptTopics.length) {
				done();
			}
		};

		// Process all at once.
		adaptTopics.forEach((t) => {
			t.push(grunt, onComplete);
		});

		return;
	});
};