module.exports = function(grunt) {
	grunt.registerTask('task-setupTopic', 'Prepares ALL Topics for SCORMing/building/EPUBing. Mostly relevant to AdaptTopics.', function(topicIdx) {
		
		let done = this.async();
		let topics = grunt.config.get("topics");

		let topicsComplete = 0;

		// Wait for all topics to be ready before ending this task.
		let onComplete = () => {
			topicsComplete++;
			if (topicsComplete == topics.length) {
				done();
			}
		};

		// Process all at once.
		topics.forEach((t, i) => {
			t.setup(grunt, onComplete);
		});

		return;
	});
};