const AdaptTopic = require('../helpers/topic/AdaptTopic');
const EdgeTopic = require('../helpers/topic/EdgeTopic');

module.exports = function(grunt) {
	grunt.registerTask('build-setupData', 'Using the data retrieved from grunt.config.get("buffetFilename"), store information about the content thats about to be created.', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');
		const _ = require('lodash');
		const JSON5 = require('json5'); // Out JSON may have /* comments */ in it.
		let Helpers = require('../helpers')(grunt);

		const buffetDir = grunt.config.get("buffetDir");
		const buffetFilename = 	grunt.config.get("buffetFilename");
		const buffetData = 		grunt.config.get("buffetData"); // Passed in by prompt:chooseTins

		// Store the user's choice in the save file.
		if (buffetData) {
			let tinList = buffetData.topics.map((e) => {return e.tin;});
			Helpers.saveRecent(tinList.join(","));
		}
		else {
			Helpers.saveRecent(buffetFilename);
		}
		
		const allTins = grunt.config.get("tins"); // Loaded with 'build-loadTins'.

		const buildData = buffetData || JSON5.parse(grunt.file.read(path.join(buffetDir, buffetFilename)));

		// Get the default settings for each topic.
		let topicSettings = buildData.topicSettings;
		
		Helpers.logBanner("Build info");
		grunt.log.writeln(chalk.bgYellow(chalk.black("Production code:")) + " " + chalk.whiteBright(buildData.productionCode));
		grunt.log.writeln(chalk.bgYellow(chalk.black("Build version:  ")) + " " + chalk.whiteBright(buildData.version));
		
		// Check each topic's data.
		// -----------------------------------------------------
		Helpers.logBanner("Checking that topic data exists...");
		grunt.log.writeln(chalk.bgWhiteBright(chalk.black("(A)dapt")) + "\n" + chalk.bgBlueBright(chalk.black("(E)dge ")) + "\n");

		let topics = [];
		let topicIDs = [];

		/**
		 * Multiple ContentObjects clause:
		 * ------------------------------
		 * Some Adapt topics contain multiple sub-topics within them; each stored in its own contentObjects.json element.
		 * 
		 * For example:
		 * 
		 * {"ain": "000001", "src": "R:/Production/Working_files/ShortCourse/GEN5ITB/GEN5ITB.A1", "modifiers": ["--co=000001"]},
		 * {"ain": "000002", "src": "R:/Production/Working_files/ShortCourse/GEN5ITB/GEN5ITB.A1", "modifiers": ["--co=000002"]},
		 * {"ain": "000003", "src": "R:/Production/Working_files/ShortCourse/GEN5ITB/GEN5ITB.A1", "modifiers": ["--co=000003"]},
		 * 
		 * The above is a SINGLE topic which contains THREE topics within it.
		 * 
		 * Buffet bugs out when encountering these (due to the shared src). To resolve this; all but the first topic with the 
		 * same "src" and with a "--co=**" modifier is skipped.
		 */
		let _checkedCoSrcs = [];

		buildData.topics.forEach(topic => {

			// Extend default topic settings with the individual topic's settings.
			topic = _.extend({}, topicSettings, topic);

			// If a TIN is being used, have the topic data be a merge.
			if (topic.tin) {

				// Modifiers: make sure the modifiers are COMBINED, not overridden/merged.
				let m = (allTins[topic.tin].modifiers || []).concat(topic.modifiers || []);

				topic = _.merge({}, allTins[topic.tin], topic);
				topic.modifiers = _.uniq(m);
				
				// FIXME: May have unpredictable results with a lot of duplicate modifiers provided.
				// DO NOT DO THIS THOUGH \/
				//_.uniq(m.reverse()).reverse(); // Use 'reverse()' ensure that LAST instance of duplicated modifiers is kept, NOT the first.
			}

			let topicId = topic.tin || topic.ain || topic.num;

			// Multiple contentObjects clause.
			let coModifier = topic.modifiers?.find(e => {return e.match(/^\s*--co\s*=/g);});
			if (coModifier) {
				if (_checkedCoSrcs.includes(topic.src)) {
					grunt.log.writeln(chalk.blueBright(`              Skipped ${topicId} due to its '${coModifier}' modifier.`));
					grunt.log.writeln("");
					return;
				}
				_checkedCoSrcs.push(topic.src);
			}

			// Check for undefined TIN numbers
			if (topicId === undefined) {
				grunt.fail.fatal(`One or more topics in your *.buffet don't have their "tin"/"ain" value defined.
If you are not using a "tin"/"ain" value; the "num" value MUST be defined!
Please update your *.buffet file and try again.`);
				return;
			}

			// Check for duplicate tin numbers
			if (topicIDs.includes(topicId)) {
				grunt.fail.fatal(`Duplicate topic TIN detected: ${topic.tin}
Please update your *.buffet file and try again.`);
				return;
			}
			topicIDs.push(topicId);

			let t = null;

			switch (topic.contentType) {
				case "adapt":
					t = new AdaptTopic({
						tin: topic.tin || "",
						ain: topic.ain || "",
						num: topic.num,
						src: topic.src,
						isAvailable: !(topic.isAvailable === false),
						fwk: topic.fwk,
						modifiers: topic.modifiers,
						localRepoDir: grunt.config.get("localAdaptRepoDir"),
						productionCode: buildData.productionCode || ""
					});
					break;

				case "edge":
				default:
					t = new EdgeTopic({
						tin: topic.tin || "",
						ain: topic.ain || "",
						num: topic.num,
						src: topic.src,
						isAvailable: !(topic.isAvailable === false),
						modifiers: topic.modifiers,
						productionCode: buildData.productionCode || ""
					});
					break;
			}
			
			t.load();
			// FIXME: If this isn't done, for some reason the object loses its functions when referred to outside of this task.
			let fns = Object.getOwnPropertyNames(Object.getPrototypeOf(t));
			fns.forEach(fn => {
				t[fn] = t[fn].bind(t);
			});

			// Don't push unavailable topics.
			if (t.isAvailable !== false) {
				topics.push(t);
			}

			grunt.log.writeln("");
		});

		grunt.config.set("topics", topics);
		grunt.config.set("buildData", buildData);

		// If there are no available topics, stop the program.
		if (!topics.length) {
			grunt.fail.fatal("No available Topics found to proceess. Please check your topics / *.buffet file and try again.");
			process.exit(1); // Endpoint reached. Stop all grunt tasks.
		}

		return;
	});
};