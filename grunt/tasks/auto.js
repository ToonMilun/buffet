module.exports = function(grunt) {

	/**
	 * Processes a single *.buffet file from the command line.
	 * (Auto-answers prompts based on user input).
	 * 
	 * Note: grunt-clean task will be skipped.
	 * 
	 * TODO: At the moment, there's no way to specify for only specific topics to be processed (ALL topics are processed each time).
	 */
	grunt.registerTask('auto', [

		'build-loadTins',			// Load in the base TIN data from 'tins.buffet'.
		'auto-setup'				// Process the user's command line arguments.

	]);
};