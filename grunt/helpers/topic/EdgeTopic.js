const Topic = require('./Topic');
const path = require('path');
const chalk = require('chalk');

const execFile = require('child_process').execFile;
const xml2js = require('xml2js').parseString;

/**
 * Handles all necessary operations to process an Edge Topic.
 */
class EdgeTopic extends Topic {

  /**
   * @param {Object} options
   * @param {number} options.num    // Topic number
   * @param {string} options.src    // Source
   * @param {[string]} options.modifiers    // Modifiers
   * @param {string} options.isAvailable    // If !isAvailable, topic will not be processed.
   */
  constructor({
    tin = "",
    ain = "",
    num = 1,
    productionCode = "",
    src = "UNDEFINED",
    modifiers = [],
    isAvailable = true,
  } = {}) {
    super({ tin, ain, num, productionCode, src, modifiers, isAvailable });

    this.type = "edge";
    this.bgColor = "bgBlueBright";
  }

  validateSrc() {

    // Check that the "src" files contain a "imsmanifest.xml" file in the root (all Edge src's should have that).
    if (!this._logCheckPathExists(path.join(this.src, "imsmanifest.xml"), `src: ${this.src}`)) return false;
    
    return true;
  }

  setup(grunt, callback) {
    this._hasShownHeader = false;
    this.logHeader("Skipped! (Edge Topics don't need any setting up in order to SCORM them)");
    callback();
  }

  /**
   * To SCORM an Edge topic:
   * -----------------------
   * 1. Create a single zip containing all files in the "this.src" directory.
   * 2. In the ZIP, replace all "X.xml" files with their "X--modifier.xml" counterparts (if they exist).
   *    Do this ONLY for the XMLs found in the root and "section1" directory (do NOT go deeper).
   * 
   * WARNING: This makes parameters for and uses a batch script.
   * WARNING: At this time, the batch script being executed cannot ECHO anything to the cmd. Errors seem to echo just fine however.
   * 
   * @param {*} outputDir 
   */
  scorm(grunt, callback) {

    this._hasShownHeader = false;

    // Find all XML files which have one or more "--modifier" in their name that are ALSO in "this.modifiers".
    let modFiles = [];
    let logFiles = []; // Logged to the console.

    this.modifiers.forEach(modName => {

      grunt.file.expand({filter: 'isFile', cwd: this.src}, 
        [
          // Example modifiers: [--ke02, --ke02-t12]
          // Example file:      unitinfo--ke02-t12.xml

          // Additional check:
          // Prevent partial matches (in the example above, the file should NOT be matched to the "--ke02" modifier, because "--ke02-t12" is not the same thing.).

          path.join(`**${modName}--**.*`), // Match the modifier if it's not the last modifier in the name (example: unitinfo--ke02--ke13.xml)
          path.join(`**${modName}.*`),     // Match the modifier if it IS the last modifier in the name (example: unitinfo--ke13--ke02.xml)
          path.join("section1", `**${modName}--**.*`),
          path.join("section1", `**${modName}.*`)
        ]
      ).forEach((p) => {

        modFiles.push(p); // All modfile paths are relative to the root of the .zip that will be created.
        logFiles.push(path.dirname(p) + "/" + chalk.blueBright(path.basename(p)));
      });
    });

    // Log which modifier files are about to be applied to the topic.
    this.logHeader(`Modifier files found:`);
    this.logHeader(`${logFiles.join(", ")}`);

    grunt.log.writeln();


    // EXPERIMENTAL. DOES NOT WORK THROUGH R: DRIVE!
    /*if (grunt.config.get("adaptBuildMode") == "upload") {
      this.upload(grunt);
      return;
    }*/

    // Zip the contents. Tried using NPM extensions; only execFile seemed to work.
		/*execFile('echo', ['TESTING TESTING TESTING'], function(err, stdout) {

			if (err) {
				grunt.fail.fatal(err);
				return;
			}

			grunt.log.writeln(stdout);
		});

    return;*/

    // ZIP the entire "this.src". Needs to be done using a cmd command.
    // ----------------------------------------------------------------
    // (All paths are wrapped in "" as the Batch script will misinterpret whitespace characters as indicating new parameters).
    this.logHeader(`Zipping files to ${chalk.magentaBright(grunt.config.get("outputDir") + "/" + this.id + ".zip")} (This will open in a popup window, and likely take a while).`);

    // Batch has a limit of 255 parameters. If more modified files than that exist (this shouldn't ever happen), throw an error.
    if (modFiles.length > 252) {
      grunt.fail.fatal(`EdgeTopic: a maximum of 252 modifier files are allowed in a topic (you have ${modFiles.length}).`);
    }

    // Add all modFiles to zipEdgeArgs (%4% onwards).
    // Each modFile's path is wrapped in "" (if the filepath has a whitespace character in it; which they often do, this prevents the batch script from thinking it's a parameter break).
    for (let i = 0; i < modFiles.length; i++) modFiles[i] = `"${modFiles[i]}"`;

    const zipName = (this.productionCode ? (this.productionCode + ".") : "") + this.id;

    this._scorm(
      grunt,
      this.src,
      path.resolve(path.join(grunt.config.get("outputDir"), `${zipName}.zip`)),
      this.num || 0,
      modFiles,
      callback);

    return true;
  }

  /** Parses an XML string into a JavaScript object. */
  parseXML(xmlString) {
    return new Promise(resolve => {

      xml2js(xmlString, function(err, result) {
        resolve(result);
      });
    });
  }

  async _scorm(grunt, dir, output, num, mods, callback) {
    
    // Wait for any other EdgeTopics to finish SCORMing this repo.
    await this._waitAndLockRepo(dir);

    //let imsmanifest = grunt.file.read(path.join(dir, "imsmanifest.xml"));
    let unitInfo = grunt.file.read(path.join(dir, "section1", "unitinfo.xml"));
	if (!unitInfo) grunt.fail.fatal(`Failed to read 'unitinfo.xml' from within the Edge unit!`);

    let data = await this.parseXML(unitInfo);
    const unitname = data.unitinfo.generalinfo[0].unitname[0];
    const topicname = data.unitinfo.generalinfo[0].topicname[0];

	if (!unitname) grunt.fail.fatal(`Could not read 'unitname' from unitinfo.xml!`);
	if (!topicname) grunt.fail.fatal(`Could not read 'topicname' from unitinfo.xml!`);

    // Generate the imsmanifest.xml.
    // FIXME: Hardcoded due to time constraints.

    let imsmanifestXML = `<?xml version="1.0" encoding="utf-8" standalone="no" ?> 

<manifest identifier="com.didasko.sit.${this.productionCode}_T${num}" version="2.0"
			xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2" 
			xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2" 
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
			xsi:schemaLocation="http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd"> 
	<metadata> 
		<schema>ADL SCORM</schema> 
		<schemaversion>1.2</schemaversion> 
		<lom xmlns="http://www.imsglobal.org/xsd/imsmd_rootv1p2p1"> 
			<general> 
				<title> 
					<langstring>${this.productionCode}</langstring> 
				</title> 
				<catalogentry> 
					<catalog></catalog> 
					<entry><langstring xml:lang="en"></langstring></entry> 
				</catalogentry> 
				<description> 
					<langstring xml:lang="en">.</langstring> 
				</description> 
				<keyword> 
					<langstring xml:lang="en">${unitname}</langstring> 
				</keyword> 
			</general> 
			<lifecycle> 
				<version> 
					<langstring xml:lang="x-none">1</langstring> 
				</version> 
				<status> 
					<source> 
						<langstring xml:lang="x-none">LOMv1.0</langstring> 
					</source> 
					<value> 
						<langstring xml:lang="x-none">Final</langstring> 
					</value> 
				</status> 
			</lifecycle> 
			<metametadata> 
				<metadatascheme>LOMv1.0</metadatascheme> 
			</metametadata> 
			<technical> 
				<format>text/html</format> 
				<location type="URI">http://www.didasko.com</location> 
			</technical> 
			<educational> 
				<typicallearningtime> 
					<datetime>01:00:00</datetime> 
				</typicallearningtime> 
			</educational> 
			<rights> 
				<cost> 
					<source> 
						<langstring xml:lang="x-none">LOM-WD6.1</langstring> 
					</source> 
					<value> 
						<langstring xml:lang="x-none">no</langstring> 
					</value> 
				</cost> 
				<copyrightandotherrestrictions> 
					<source> 
						<langstring xml:lang="x-none">LOM-WD6.1</langstring> 
					</source> 
					<value> 
						<langstring xml:lang="x-none">no</langstring> 
					</value> 
				</copyrightandotherrestrictions> 
				<description> 
					<langstring xml:lang="en"></langstring> 
				</description> 
			</rights> 
			<classification> 
				<purpose> 
					<source> 
						<langstring xml:lang="x-none">LOM-WD6.1</langstring> 
					</source> 
					<value> 
						<langstring xml:lang="x-none">Educational Objective</langstring> 
					</value> 
				</purpose> 
				<description> 
					<langstring xml:lang="en"></langstring> 
				</description> 
				<keyword> 
					<langstring xml:lang="en"></langstring> 
				</keyword> 
			</classification> 
		</lom> 
	</metadata> 
	<organizations default="${this.productionCode}_T${num}_org" > 
		<organization identifier="${this.productionCode}_T${num}_org" > 
			<title>${unitname}</title> 
			<item identifier="${this.productionCode}_T${num}_1_idef" identifierref="${this.productionCode}_T${num}_1_idef_ref"> 
				<title>${topicname}</title> 
			</item> 
		</organization> 
	</organizations> 
	<resources> 
		<resource identifier="${this.productionCode}_T${num}_1_idef_ref" type="webcontent" adlcp:scormtype="sco" href="section1/index.html">  
			<file href="section1/index.html" /> 
		</resource> 
	</resources> 
</manifest>`;

	this.logHeader(`imsmanifest.xml generated successfully.`);
	this.logHeader(`unitcode:  ${this.productionCode}`);
	this.logHeader(`unitname:  ${unitname}`);
	this.logHeader(`topicname: ${topicname}`);
	this.logHeader(`topicnum:  ${num}`);

	// Overwrite the existing imsmanifest.xml on the R: drive.
	grunt.file.write(path.join(dir, "imsmanifest.xml"), imsmanifestXML);


    // IMPORTANT! All path values (below) are wrapped in ".
    // This is done to prevent batch errors if the path contains a whitespace character (which is often the case with Edge topics).
    let s = new Promise(resolve => {
      grunt.util.spawn({
        cmd: 'grunt',
        args: [
          "run:zip-edge",
          `--dir="${dir}"`,
          `--output="${output}"`,
          `--num=${num}`,             // FIXME: Completely unused (do not remove without editing batch too!).
          `--mods=${mods.join("#")}`  // %4% onwards. Arrays need to be joined with "#" (popup.bat splits on this char).
        ]
      }, (error, result, code) => {
        if (error) console.log(chalk.redBright(error.toString()));
        console.log(result.toString());
        resolve(true);
      });
    });
    await s;

    // Replace the files in the .zip with the ones from the 'extensions' directory.
    await this.extensionsReplaceInZip(grunt, output);

    // Unlock this directory (allow for other EdgeTopics to SCORM it).
    this.unlockRepo(dir);

    callback();
  }

  // Extensions
  // ---------------------------------------
  // For each file in extensions/edge/replace, find ALL files in this topic that have the same name, and record them.
  extensionsReplaceInZip(grunt, zipDir) {

    let _this = this;

    // Get the list of files from within extensions/edge/replace.
    const replacePath = path.join(grunt.config.get("extensionsDir"), "edge", "replace");
    const replaceNames = grunt.file.expand({filter: 'isFile', cwd: replacePath}, "**");

    const TMP = `__tmp${this.id}`; // As multiple Edge SCORMs could be happening at once, give each of them their own tmp directory.
    if (grunt.file.exists(TMP)) {
      this.logHeader(`Deleting ${TMP} directory...`);
      grunt.file.delete(TMP);
    }

    grunt.log.writeln("");
    this.logHeader(chalk.yellowBright(`Replacing files in .zip with the files from '${replacePath}'...`));

    return new Promise(resolve => {

      // Now, find them in the zip.
      // The following cmd command will list ALL files in the .zip, in the following format (example):
      // 2014-01-24 14:45:56 ....A         4508          795  adlcp_rootv1p2.xsd
      execFile('7z', ['l', '-ba', zipDir], function(err, stdout) {

        if (err) {
          grunt.fail.fatal(err);
          resolve();
          return;
        }

        let allFilePaths = [];

        let rows = stdout.split('\n').map(r => {return r.trim();});
        rows.forEach(row => {
          
          // Get the filename from the row (note: this regex should still select it even if the filename has space(s) in it).
          let match = row.match(/^.*?[\d]+\s+[\d]+\s+(.*)$/);
          if (!match) return;
           
          allFilePaths.push(match[1]);
        });

        // Now that all filenames have been found, determine which ones need to be replaced.
        let filePaths = allFilePaths.filter(p => {return replaceNames.includes(path.basename(p));});

        // Due to how the '7z' command works, in order to replace a file, the exact structure must be made in the root.
        // Copy all the files to a 'tmp' directory.
        filePaths.forEach(p => {
          grunt.file.copy(path.join(replacePath, path.basename(p)), path.join(TMP, p));
        });

        // Navigate into the TMP folder (important!), then go through the .zip and replace them.
        execFile('7z', ['u', zipDir, "*"], {cwd: TMP}, function(err, stdout) {
          
          // Delete the 'tmp' dir.
          //_this.logHeader(`Deleting ${TMP} directory...`);
          grunt.file.delete(TMP);

          if (err) {
            grunt.fail.fatal(err);
            resolve();
            return;
          }

          _this.logHeader(chalk.greenBright(`Successfully replaced the following files (${filePaths.length} found):`));
          _this.logHeader(chalk.greenBright(`${replaceNames.join(', ')}`));

          resolve();
        });
		  });
    });
  }

  /**
   * Uploads the Edge topic to a TIN url.
   * @param {*} grunt 
   */
  upload(grunt) {

    if (!this.tin && !this.ain) throw `EdgeTopics.js: cannot upload an Edge topic with no TIN number assigned.`;

    const LOG_SIZE = 50;
    const proofingURL = this.tin ? `TINs/${this.tin}` : `AINs/${this.ain}`;
    const fs = require('fs');

    // Get filelist
		// ------------------------------------------------
		let filelist = [];
		grunt.file.expand({filter: "isFile"}, path.join(this.src, '/**')).forEach(function(p) {
					
			let stats = fs.statSync(p);
			let localSize = stats.size;
			let remoteUrl = path.relative(outputdir, p);

			filelist.push({
				srcPath: p,
				url: remoteUrl,
				size: localSize
			});
		});

    return;

		// FTP
		// ------------------------

		const ftp = require("basic-ftp");
		let done = this.async();

		async function example() {
			const client = new ftp.Client();
			client.ftp.verbose = false;

			try {
				await client.access({
					host: "proofing.didaskogroup.com",
					user: "dsko.proofing",
					password: "Le46Cc7cbQHwYYUh",
					secure: false
				})
				//grunt.log.writeln(await client.list());

				// Custom progress logging for the file transfers.
				client.trackProgress(info => {

					// Sometimes this gets logged twice, with the first instance being 0 bytes.
					// Skip logging in that case.
					if (info.bytes == 0) return;

					let dashes = new Array(LOG_SIZE - info.name.length + 1).join("-");

					grunt.log.writeln(chalk.blueBright(`\r${info.name} ${dashes} size: ${info.bytes}`));
					/*
					console.log("File", info.name)
					console.log("Type", info.type)
					console.log("Transferred", info.bytes)
					console.log("Transferred Overall", info.bytesOverall)*/
				})

				

				// Create and then navigate into the specified folder for uploading.
				await client.ensureDir(proofingURL);
				let root = await client.pwd();
				let cdPrev = "";

				// To save time, only upload files which have a different filesize to the ones already uploaded (if they exist).
				for (let i = 0; i < filelist.length; i++) {

					grunt.log.write(chalk.black(chalk.bgYellowBright(`\r${i+1} / ${filelist.length}`)));

					let cd = path.join(root, path.dirname(filelist[i].url));
					cd = cd.replace(/[\\]/g, "/");

					// Change the client.cd only when a directory change occurs.
					if (cdPrev != cd) {
						cdPrev = cd;

						//await client.cd(root);

						// Navigate into the directory (and create it if it doesn't exist).
						await client.ensureDir(cd);
					}

					let fileUrl = path.basename(filelist[i].url);

					// Check if the size of the file on Proofing is different to the size of the file locally,
					// and upload if that's the case. 

					// FIXME: It's possible that sometimes, both client.uploadFrom()s below get called for the same file.
					try {
						let remoteSize = await client.size(fileUrl);

						// Upload if the filesize is different, or if the file is .json (or .jsonsynt).
						if (path.extname(fileUrl).match(/^.json/) || filelist[i].size !== remoteSize) {
							await client.uploadFrom(filelist[i].srcPath, fileUrl);
						}
					}
					catch (err) {
						// Error was thrown (likely because the file doesn't exist on the FTP yet). Upload.
						await client.uploadFrom(filelist[i].srcPath, fileUrl);
					}
				}

				//await client.uploadFromDir(outputdir);
				grunt.log.writeln("");
				done();
			}
			catch(err) {
				grunt.log.error(err);
			}
			client.close();
		}

		example();
  }

  resetHeader(...args) {super.resetHeader(...args);}
  logHeader(...args) {super.logHeader(...args);}
  errorHeader(...args) {super.errorHeader(...args);}
  failHeader(...args) {super.failHeader(...args);}
  checkCanSkip(...args) {super.checkCanSkip(...args);}
}

module.exports = EdgeTopic;
