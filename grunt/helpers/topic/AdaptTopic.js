const Topic = require('./Topic');
const path = require('path');
const chalk = require('chalk');
const simpleGit = require('simple-git');
const fs = require('fs-extra');

const { GitPluginError } = require('simple-git');
const { assert } = require('console');

// This is the URL for the most up-to-date version of the framework.
// It will be added as a secondary remote to all framework repositories.
//const fwkCurrentRepo = "https://ToonMilun@bitbucket.org/ToonMilun/adapt_v4.git";

/**
 * Handles all necessary operations to process an Adapt Topic.
 */
class AdaptTopic extends Topic {

  /**
   * @param {Object} options
   * @param {number} options.num    // Topic number
   * @param {string} options.src    // Source
   * @param {[string]} options.modifiers    // Modifiers
   * @param {string} options.isAvailable    // If !isAvailable, topic will not be processed.
   * 
   * Exclusive to Adapt Topics:
   * @param {string} options.fwk    // Framework
   */
  constructor({
    tin = "",
    ain = "",
    num = 1,
    productionCode = "",
    src = "UNDEFINED",
    modifiers = [],
    isAvailable = true,
    fwk = "UNDEFINED",
    localRepoDir = "UNDEFINED"
  } = {}) {
    super({ tin, ain, num, productionCode, src, modifiers, isAvailable });

    this.fwk = fwk;
    this.type = "adapt";
    this.bgColor = "bgWhiteBright";
    this.localRepoDir = localRepoDir;

    // All AdaptTopics will be cloned inside of this directory.
    this.gitDirName = path.basename(this.src); // Get the name of the directory on the R: drive.
                                               // Examples:
                                               // IT06.1 (legacy naming convention)
                                               // IT06_1 (legacy naming convention)
                                               // IT06--LTU__TIN-00087.

    let groupName = this.gitDirName.match(/^([\w-]+?)[._]/);
    let gName = (!groupName) ? null : groupName[1] || groupName[2];

    // Check that the group name was derrived correctly.
    if (!gName || gName == this.gitDirName) {

      let err = `
ERROR: Incorrectly formatted Adapt "src" directory name: ${this.gitDirName}
Adapt "src" repos must be named following one of the following formats:
IT06.1       (legacy)
IT06_1       (legacy)
IT06--LTU__TIN-00087`;

      console.log(err);
      throw err;
    }

    this.gitFwkDir = path.join(this.localRepoDir, gName, this.gitDirName);
    this.gitSrcDir = path.join(this.gitFwkDir, "src", "course", "en");
  }

  /** 
   * Checks the files in the "src" AND in the "fwk" to ensure that they exist and the urls are pointing to the right location.
   * @returns {Boolean}
   */
  validateSrc() {

    // Check that the "src" files contain a "components.jsonsynt" or "components.jsynt" file in the root (all Adapt src's should have that).
    if (
      !this._logCheckPathExists(
        fs.existsSync(path.join(this.src, "components.jsynt")) ? path.join(this.src, "components.jsynt") : 
        path.join(this.src, "components.jsonsynt"), 
        `src: ${this.src}`)      
      ) return false;

    // Check that the "fwk" files contain an "adapt.json" file in the root (all Adapt frameworks should have that).
    //if (!this._logCheckPathExists(path.join(this.fwk, "adapt.json"), `fwk: ${this.fwk}`)) return false;

    // Log the location where the Adapt will be cloned to.
    this.logHeader(`git: ${this.gitFwkDir} ${chalk.yellow("(The above will be cloned into here)")}`);
    
    return true;
  }

  /**
   * To SETUP an Adapt topic:
   * ------------------------
   * 1. The topic's framework AND its source both need to be cloned to the user's local drive.
   *    If this task has already been performed once, instead just call a "pull" to both of those locations.
   * 2. Ensure that the Adapt topic can be built (Adapt topics need to have had "npm install" run on them).
   * 
   * FIXME: For some reason, setting the function below to be async makes "task-setupTopic.js" unable to to access it.
   * The following is the workaround using a callback.
   * 
   * @param {*} outputDir 
   */
  setup(grunt, callback) {
    this._hasShownHeader = false;
    this._setup(grunt, callback);
  }

  /**
   * To SCORM an Adapt topic:
   * ------------------------
   * 1. Assume that the topic can be built (this was checked in the "setup" step).
   * 2. "grunt build --modifier(s)" needs to be run on the Adapt topic.
   * 3. The contents of the Adapt topic's "build" folder need to be .zipped and moved to "output/${this.num}.zip"
   * 
   * FIXME: For some reason, setting the function below to be async makes "task-scormTopic.js" unable to to access it.
   * The following is the workaround using a callback.
   * 
   * @param {*} outputDir 
   */
  scorm(grunt, callback) {

    this._hasShownHeader = false;
    this._scorm(grunt, callback);
    
    return;
  }

  push(grunt, callback) {
    this.resetHeader();
    this._push(grunt, callback);
    return;
  }

  async _push(grunt, callback) {
    
    // Only apply the repo fix if the clone hasn't been made yet (ensures it's only applied once).
    //if (!grunt.file.exists(path.join(this.src, ".git"))) {
    this.logHeader(`Committing all files from:  ${chalk.blueBright(this.gitSrcDir)}`);

    await this._waitAndLockRepo(this.gitSrcDir);

    let commitPush = async () => {
      return new Promise(resolve => {
        grunt.util.spawn({
          cmd: 'grunt',
          args: ["run:git-commit-pull-push", `--dir="${this.gitSrcDir}"`, `--mods=${this.modifiers.join("#")}`] // Arrays need to be joined with #.
        }, (error, result, code) => {
          if (error) console.log(chalk.redBright(error.toString()));
          console.log(result.toString());
          resolve();
        });
      });
    }
    await commitPush();

    this.unlockRepo(this.gitSrcDir);

    callback();
  }

  async _scorm(grunt, callback) {

    // FIXME: very rushed.
    let buildCommand;
    let run;

    const zipName = (this.productionCode ? (this.productionCode + ".") : "") + this.id;
    let output = path.resolve(path.join(grunt.config.get("outputDir"), `${zipName}.zip`));

    switch (grunt.config.get("adaptBuildMode")) {
      case "epub":
        buildCommand = "epub";
        run = "epub-adapt";
        break;

      case "upload":
        buildCommand = "build-upload";
        run = "build-upload-adapt";
        break;

      case "uploadAA": 
        buildCommand = "dev-upload";
        run = "dev-upload-adapt";
        grunt.log.warn(chalk.bgRedBright(chalk.whiteBright("WARNING! Modifiers will NOT be applied!")));
        break;

      case "scormAA": 
        buildCommand = "build-scorm";
        run = "build-scorm-adapt";
        grunt.log.warn(chalk.bgRedBright(chalk.whiteBright("WARNING! Modifiers will NOT be applied!")));
        output = path.resolve(grunt.config.get("outputDir")); // Multiple files are being moved. Provide only the directory.
        break;

      default: 
        buildCommand = "build";
        run = "build-adapt";
    }

    const gruntCommand = (`grunt ${buildCommand} ${this.modifiers.join(" ").trim()}`);

    /**
     * WARNING: Batch script.
     * 
     * %1%: this.gitFwkDir
     * %2%: Location to move the .zip created after building the Adapt topic.
     * $3%: ALL this.modifiers joined with ' ', intentionally surrounded by " " characters
     *      (otherwise, Batch will interpret the ' ' as indications that a new parameter has started).
     */
    this.logHeader(`Running: ${chalk.magentaBright(gruntCommand)} (This will open in a popup window).`);
    
    // Lock this repo while it's being built.
    await this._waitAndLockRepo(this.gitFwkDir);

    
    
    let s = new Promise(resolve => {
      grunt.util.spawn({
        cmd: 'grunt',
        args: [
          `run:${run}`,
          `--dir="${this.gitFwkDir}"`,
          `--output="${output}"`,
          `--mods=${this.modifiers.join("#")}` // Arrays need to be joined with #.
        ]
      }, (error, result, code) => {
        if (error) console.log(chalk.redBright(error.toString()));
        console.log(result.toString());
        resolve(true);
      });
    });
    await s;

    this.unlockRepo(this.gitFwkDir);

    /*let batchArgs = [
      this.gitFwkDir,
      `"${path.resolve(path.join(grunt.config.get("outputDir"), `${this.num}.zip`))}"`, // %2%
      `"${this.modifiers.join(" ")}"`
    ];
    grunt.config.set("_buildAdaptArgs", batchArgs);
    grunt.task.run("commands:build-adapt");*/

    callback();
  }

  async _setup(grunt, callback) {

    // Clone both repos that this Topic uses to a location on the local drive, then callback.
    await this._clone(grunt);
    
    callback();
  }

  /**
   * This is the callback for each tick of simpleGit processes.
   * To save on space, the \r was added to have each subsequent callback overwrite the previous one's log.
   * @param {*} event 
   */
  gitProgress(event) {
    console.log(`\r${this.headerPadding}git:${event.method} ${event.stage} stage ${event.progress}% complete`);
  }

  gitError(error, result) {

    // optionally pass through any errors reported before this plugin runs
    if (error) return error;

    // customise the `errorCode` values to treat as success
    if (result.errorCode === 0 || !result.errorCode) {
       return;
    }

    console.log(result.errorCode);

    // the default error messages include both stdOut and stdErr, but that
    // can be changed here, or completely replaced with some other content
    return Buffer.concat([...result.stdOut, ...result.stdErr]);
  }

  /**
   * To be used inside _clone().
   * @returns {Boolean} true if the repo existed when this function was called; false if otherwise.
   */
  async _cloneRepo(grunt, repoDir, localDir) {

    this.resetHeader();

    return new Promise(async (resolve, reject) => {

      // Remote gits
      const repoGit = simpleGit({
        progress: this.gitProgress,
        errors: this.gitError
      });

      // Local gits (these will be made).
      const localGit = simpleGit({
        baseDir: localDir,
        progress: this.gitProgress,
        errors: this.gitError
      });

      // Create a new clone
      // ------------------------------------------------------------------------------
      // If the local repo doesn't exist, clone the remote repo there.
      // (Check if the local repo exists by looking for the ".git" directory within it).
      if (!grunt.file.exists(path.join(localDir, ".git"))) {

        this.logHeader(chalk.yellowBright(`${chalk.blueBright("**/" + path.basename(localDir))} has not been created yet. Cloning... (this will take a long time if you're on the VPN)`));

        // Check if the directory is empty, and throw an error if it isn't.
        if (grunt.file.expand(path.join(localDir, "*")).length) {
          reject(
            `Could not clone Adapt into directory: ${localDir}"
  The directory is not empty, nor is it a GIT repository. Please clear it and try again.`);
          return;
        }

        // Try to clone.
        // IMPORTANT: A shallow clone is deliberately made (GREATLY improves cloning time).
        try {

          //await repoGit.clone(repoDir, localDir);
          //await repoGit.clone(repoDir, localDir, {'--depth': 1});

          // WARNING: Batch.
          // This will run a batch script in a new CMD window, WAIT FOR IT TO COMPLETE, then call the callback.
          grunt.util.spawn({
            cmd: 'grunt',
            args: ["run:git-clone", `--repo="${repoDir}"`, `--dir="${localDir}"`]
          }, (error, result, code) => {
            if (error) console.log(chalk.redBright(error.toString()));
            console.log(result.toString());
            resolve(true);
          });

        }
        // Show an error if something goes wrong (such as the repo already containing something).
        catch (err) {
          reject(err);
          return;
        }
      }
      // Pull to update to an existing local repo.
      // ------------------------------------------------------------------------------
      else {

        this.logHeader(chalk.yellowBright(`${chalk.blueBright("**/" + path.basename(localDir))} already exists. Pulling...`));
        
        // Check that the "origin" remote for the local repo matches "repoDir".
        let localOrigin = await localGit.getConfig("remote.origin.url");
        localOrigin = localOrigin.value;

        // Compare the paths (ignoring differences between / and \ characters).
        if (path.resolve(localOrigin) != path.resolve(repoDir)) {
          this.resetHeader();
          this.errorHeader("ERROR: " + localDir);
          reject(chalk.red(`Repo origin mismatch!

Your ${chalk.yellowBright("*.buffet")} file specifies the following 'origin':         ${chalk.yellowBright(repoDir)}
The existing repo in your ${chalk.blueBright("local")} directory has this 'origin': ${chalk.magentaBright(localOrigin)}

To fix this:
------------
- Ensure that the ${chalk.blueBright("LOCAL")} repo (${chalk.blueBright(localDir)}) content matches the content in the repo specified in your ${chalk.yellowBright("*.buffet")} file.
- If it DOES, then change the ${chalk.blueBright("LOCAL")} repo's 'origin' to ${chalk.yellowBright(repoDir)} using a git command.
- If it DOESN'T, then move/rename your ${chalk.blueBright("LOCAL")} repo and run this program again.

Contact Milton Plotkin if unsure.`));
          return;
        }

        // Try to pull.
        try {
          //await localGit.pull();

          // WARNING: Batch.
          // This will run a batch script in a new CMD window, WAIT FOR IT TO COMPLETE, then call the callback.
          grunt.util.spawn({
            cmd: 'grunt',
            args: ["run:git-pull", `--dir="${localDir}"`]
          }, (error, result, code) => {
            if (error) console.log(chalk.redBright(error.toString()));
            console.log(result.toString());
            resolve(false);
          });

        }
        // Show an error if can't pull
        // (such as if the files in there aren't an actual git repo; shouldn't happen unless you've actually messed with the
        // files in there yourself).
        catch (err) {
          reject(err);
          return;
        }
      }
    })
    .catch(error => {
      grunt.fail.fatal(error);
    });
  }

  /**
   * Clones the two repositories that this topic uses to the user's C: drive / grabs the previously cloned ones if they exist.
   * @param {*} grunt 
   */
  async _clone(grunt) {
    return new Promise(async (resolve, reject) => {
      
      // Clone the fwk (framework) repo.
      // ---------------------------------------------------
      if (!grunt.file.exists(this.gitFwkDir)) grunt.file.mkdir(this.gitFwkDir); // Ensure that the directory for the fwk exists.

      this.logHeader(`Setting up local fwk repo:  ${chalk.blueBright(this.gitFwkDir)}`);

      // Wait for any other AdaptTopics to finish using that repo.
      await this._waitAndLockRepo(this.gitFwkDir);

      // FIXME: Pulling the framework from the R: drive was just too slow.
      let isNewClone = await this._cloneRepo(grunt, this.fwk, this.gitFwkDir);

      // Add a secondary remote to the framework.
      // Allows the repo to be updated to our latest scripts.
      if (isNewClone) {
        try {
          const localGit = simpleGit({
            baseDir: this.gitFwkDir,
            progress: this.gitProgress,
            errors: this.gitError
          });
          await localGit.addRemote("current", this.fwk); 
        }
        catch (err) {
          // An error will be thrown if the "current" directory already exists. Ignore it.
        }
      }

      // Run NPM install on the fwk (framework) repo specifically
      // ---------------------------------------------------
      // This step is only performed if there is no "node_modules" directory in the local git.
      this.resetHeader();
      let isNpmInstalled = false;
      let isSrcReady = false;

      let npmsInstalled = grunt.config.get("_npmsInstalled") || [];
      let npmDir = path.resolve(path.join(this.gitFwkDir, ".."));
      
      // Don't install NPM if it has/is already being installed in this session.
      if (!npmsInstalled.includes(npmDir)) {
        
        npmsInstalled.push(npmDir);
        grunt.config.set("_npmsInstalled", npmsInstalled);

        this.logHeader(`Installing node modules (running: ${chalk.magentaBright("npm install")}). This may take a while...`);
        
        grunt.util.spawn({
          cmd: 'grunt',
          args: ["run:npm-install", `--dir="${this.gitFwkDir}"`]
        }, (error, result, code) => {
          if (error) console.log(chalk.redBright(error.toString()));
          console.log(result.toString());
          
          // Wait for both the src to be ready and npm to be installed.
          isNpmInstalled = true;
          if (isSrcReady) resolve();
        });
      }
      else {
        this.logHeader(`NPM has already been installed for: ${npmDir}. Skipped.`);
        isNpmInstalled = true;
      }

      // Unlock the repo.
      this.unlockRepo(this.gitFwkDir);

      // Clone the src (course) repo.
      // ---------------------------------------------------
      if (!grunt.file.exists(this.gitSrcDir)) grunt.file.mkdir(this.gitSrcDir); // Ensure that the directory for the course exists.

      // Wait for any other AdaptTopics to finish using this repo.
      await this._waitAndLockRepo(this.gitSrcDir);

      grunt.log.writeln();

      this.logHeader(`Setting up local src repo:  ${chalk.blueBright(this.gitSrcDir)}`);
      await this._cloneRepo(grunt, /*"file://" + */ this.src, this.gitSrcDir);

      // Unlock it once done.
      this.unlockRepo(this.gitSrcDir);

      // Wait for both the src to be ready and npm to be installed.
      isSrcReady = true;
      if (isNpmInstalled) resolve();
    })
    .catch(error => {
      grunt.fail.fatal(error);
    });
  }

  resetHeader(...args) {super.resetHeader(...args);}
  logHeader(...args) {super.logHeader(...args);}
  errorHeader(...args) {super.errorHeader(...args);}
  failHeader(...args) {super.failHeader(...args);}
  checkCanSkip(...args) {super.checkCanSkip(...args);}
}

module.exports = AdaptTopic;
