const path = require('path');
const chalk = require('chalk');
const fs = require('fs-extra');

let topicRepoLock = {}; // Global object

/**
 * Base class for all types of topics (Edge/Adapt).
 */
class Topic {

  /**
   * @param {Object} options
   * @param {string} options.tin    // Topic TIN number
   * @param {string} options.num    // Topic number (legacy; refers to the chronological order of the topic for the unit).
   * @param {string} options.src    // Source
   * @param {[string]} options.modifiers    // Modifiers
   * @param {string} options.isAvailable    // If !isAvailable, topic will not be processed.
   */
  constructor({
    tin = "",
    ain = "",
    num = 1,
    productionCode = "",
    src = "UNDEFINED",
    modifiers = [],
    isAvailable = true
  } = {}) {
    this.tin = tin;
    this.ain = ain;
    this.num = num;
    this.productionCode = productionCode;

    this.id = this.tin || this.ain || this.num; // This value will be used for the ZIP filename.

    this.src = src;
    this.isAvailable = isAvailable;
    this.modifiers = modifiers;

    
    // Protected variables
    this.type = "NONE";
    this.bgColor = "bgRedBright";
  }

  /** @returns {Topic} */
  load() {

    this.setUpHeader();

    // Show the TIN that this topic will use.
    if (this.tin) {
      this.logHeader(`tin: ${chalk.greenBright(this.tin) || chalk.yellowBright("N/A")}`);
    }
    if (this.ain) {
      this.logHeader(`ain: ${chalk.greenBright(this.ain) || chalk.yellowBright("N/A")}`);
    }

    // Show num (used for topics with no TINs).
    /*if (this.num) {
      this.logHeader(`num: ${chalk.blueBright(this.num)}`);
    }*/

    // Show the modifiers that this topic will use.
    this.logHeader(`mod: [${this.modifiers.join(", ")}]`);

    // If this topic is active (being used in the build), check its files for errors.
    if (this.isAvailable) {
      this.isAvailable = Boolean(this.validateSrc());
      if (!this.isAvailable) this.errorHeader("Something has gone wrong. Topic SKIPPED!");
    }
    else {
      this.errorHeader("Topic has \"isAvailable: false\" explicitly set in the *.buffet file. SKIPPED!");
    }

    

    return this;
  }

  setUpHeader() {

    let topicIdStr = this.id.toString().padStart(7);

    let topicContentTypeStr = "";
    switch (this.type) {
      case "adapt":
        topicContentTypeStr = "(A)";
        break;
      
      case "edge":
        topicContentTypeStr = "(E)";
        break;

      default:
        topicContentTypeStr = "ERR";
    }

    this.headerStr = " " + topicContentTypeStr + " " + topicIdStr + " ";
    this.headerPadding = "".padStart(this.headerStr.length+1, " ");
  }

  /** 
   * Checks the files in the src.
   * If the src doesn't validate, should print an error and return "false" (can optionally continue from that point).
   * 
   * Should be overridden.
   * @returns {Boolean}
   */
  validateSrc() {
    return true;
  }

  resetHeader() {
    this._hasShownHeader = false;
  }

  /** 
   * Logs an indented message to the CMD.
   * @returns {void}
   */
  logHeader(str, str2) {

    let log = "";
    if (!this._hasShownHeader) {
      this._hasShownHeader = true;
      log += chalk[this.bgColor](chalk.black(this.headerStr)) + " ";
    }
    else log += this.headerPadding;

    log += chalk.white(str);
    console.log(log);
    if (str2) {
      console.log(this.headerPadding + chalk.bgWhiteBright(chalk.black("^ " + str2)));
    }
  }

  errorHeader(str, str2) {

    let log = "";
    if (!this._hasShownHeader) {
      this._hasShownHeader = true;
      log += chalk[this.bgColor](chalk.black(this.headerStr)) + " ";
    }
    else log += this.headerPadding;

    console.log(log + chalk.redBright(str))
    if (str2) {
      console.log(this.headerPadding + chalk.bgRedBright(chalk.black("^ " + str2)));
    }
  }

  failHeader(str, str2) {
    this.errorHeader(str, str2);
    throw "Please fix the above errors, then run this program again";
  }

  _logCheckPathExists(p, message) {
    if (!fs.pathExistsSync(p)) {
      this.errorHeader(`${message}`, "Incorrect/missing directory!");
      return false;
    }
    this.logHeader(`${message}`);
    return true;
  }

  /**
   * Should be called prior to SCORMing/building this Topic.
   * @returns {Boolean}
   */
  setup(grunt) {
    // Override this with the child classes.
    return true;
  }

  /** @returns {Boolean} */
  scorm(grunt) {
    // Override this with the child classes.
    return true;
  }

  /**
   * Returns true if the task for this Topic has already been performed already.
   * At the moment, this is used only for topics with "--co=**" modifiers. 
   */
  checkCanSkip(repoDir) {

    // FIXME: Hardcoded.
    if (this.modifiers && this.modifiers.find(e => {return e.match(/^\s*--co\s*=/g);})) {
      
      this.logHeader(chalk.cyanBright(`"--co=**" modifier detected.`));

      // This topics has a "--co=**" modifier. Check if this topic is locked, and if it is, skip it.
      console.log(topicRepoLock[repoDir]);
      if (topicRepoLock[repoDir]) {
        this.logHeader(chalk.cyanBright(`This topic has already been processed. Skipping...`));
        return true;
      }
    }
    return false;
  }

  /**
   * Sometimes, multiple topics in a buffet use the SAME repo.
   * Since all Topics are processed simultaneously, this can cause problems.
   * To solve this, whenever a repo is processed by an AdaptTopic object, it needs to be "locked" from being accessed by other AdaptTopic
   * objects until the first one's task is complete.
   * 
   * @returns {Promise} Returns a modified promise object that can then be awaited and/or resolved manually.
   */
   lockRepo(repoDir) {

    // Check whether this repo is already locked (if so, throw an error; it means another AdaptTopic didn't wait its turn).
    if (topicRepoLock[repoDir]) {
      this.failHeader(`AdaptTopic tried to lock repo "${repoDir}"" when it was already locked, meaning it skipped waiting for the repo to unlock itself.`);
    }

    // Add the repoDir as a key in the locked directory, with a Promise for its value.
    let promiseResolve;
    let promiseReject;
    topicRepoLock[repoDir] = new Promise((resolve, reject) => {
      promiseResolve = (...args) => {
        topicRepoLock[repoDir] = undefined;
        resolve(...args);
      };
      promiseReject = (...args) => {
        topicRepoLock[repoDir] = undefined;
        reject(...args);
      };
    });
    topicRepoLock[repoDir].resolve = promiseResolve;
    topicRepoLock[repoDir].reject = promiseReject;

    return topicRepoLock[repoDir];
  }

  unlockRepo(repoDir) {
    if (!topicRepoLock[repoDir]) this.failHeader(`AdaptTopic tried to unlock repo "${repoDir}" when it wasn't locked. Something may have gone wrong with the async.`);
    topicRepoLock[repoDir].resolve();
  }

  async _waitAndLockRepo(repoDir) {
    return new Promise(async (resolve) => {

      let myLock = topicRepoLock[repoDir];
      
      // If the repoDir hasn't been added to topicRepoLock yet, it hasn't been accessed yet, and is therefore unlocked already.
      if (!myLock) {
        let r = {
          promise: this.lockRepo(repoDir) // The resolve object must be set up this way. If a Promise object is passed, it breaks the async.
        }
        resolve(r);
        return;
      }

      this.logHeader(chalk.cyanBright(`Another Topic is currently accessing ${chalk.blueBright(repoDir)}.`));
      this.logHeader(chalk.cyanBright(`Waiting for it to finish...`));

      // Otherwise, wait for the lock to resolve...
      await myLock;

      // Then re-lock it.
      let r = {
        promise: this.lockRepo(repoDir) // The resolve object must be set up this way. If a Promise object is passed, it breaks the async.
      }
      resolve(r);
    });
  }

  /** @returns {Boolean} */
  /*push(grunt, callback) {

    callback();

    // Override this with the child classes.
    return true;
  }*/
}

module.exports = Topic;
