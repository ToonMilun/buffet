@echo off
setlocal enableextensions enabledelayedexpansion

REM Open this task in a seperate CMD window (allows for all topics to be zipped at the same time).
REM Important to assign the parameters to variables first.
set batchName=%1%
set batchArgs=%*

REM EXAMPLE batchArgs STRING:
REM zip-edge.bat "--dir=\"R:/Production/Working_files/IT/IT02/IT02--DITPRG1PPE--CSE1OFX/Topic 4\"" "--output=\"C:\Users\mplotkin\Documents\BitBucket\buffet\output\12.zip\"" --num=12 "--mods=\"section1/unitinfo--ke02-t12.xml\"#\"section1/unitinfo--ke02-t14.xml\"#\"section1/unitinfo--ke02-t12.xml\""

REM The batchArgs string needs to be modified to remove escaped double quotes.
REM --------------------------------------------------------------------------
REM Temporarily replace escaped " in the batchArgs with a placeholder character.
set batchArgs=!batchArgs:\"=@@@!

REM Remove the remaining " that are around the parameters (example: "--dir=...").
set batchArgs=!batchArgs:"=!

REM Set the placeholders to be normal double quotes.
set batchArgs=!batchArgs:@@@="!

REM Important! grunt:run passes in "passArgs" like so:
REM %2%: --name
REM %3%: value
REM %4%: --name
REM %5%: value
REM These will be in the EXACT order specified in "config/run.js".
REM Since batch doesn't need to know the name, just ignore the even numbered parameters for now.

set /a i=1
set args=
for %%A in (!batchArgs!) do (
    
    if !i! gtr 1 (
        
        set /a j=!i!%%2

        if !j! equ 1 (

            REM Append the value + a whitespace character to the arguments.
            REM But first, replace all "#" with " " (this is how array elements are passed: el0#el1#el2).
            
            set temp=%%A
            set temp=!temp:#= !

            REM Do NOT remove the whitespace character after this line \/
            set args=!args!!temp! 
        )
    )

    set /a i=!i!+1
)

start "Popup" cmd /c "call %~dp0!batchName! !args! & exit /b 0"
REM start "test" cmd /c "call %~dp0!batchName! !batchArgs! & pause & exit /b 0"

EXIT /B 0
EndLocal
ECHO ON