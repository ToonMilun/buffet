@echo off
setlocal enableextensions enabledelayedexpansion

REM There seems to be no way of doing this from within Grunt.
REM Zip up the provided directory, and then rename the modifier files within it.

REM %1%     = Input directory to zip.
REM %2%     = Output directory and final .zip filename (move the zip into here).
REM %3%     = Topic number (unused at this time)
REM %4%+    = Location (relative to the .zip root) of a modifier file that needs to replace a non-modifier file.

REM ZIP to this filename before renaming
set tempFilename=__buffet-tmp.zip
set dir=%1%

echo dir: !dir!

REM Log which files are going to override existing files.
echo modFiles:
set /a j=1
for %%A in (%*) do (
    if !j! gtr 3 (
        echo     %%A
    )
    set /a j=!j!+1
)
ECHO.

REM Navigate into the input directory.
cd /D !dir!

REM Remove any previous temp .zips (in case something went wrong)
del !tempFilename!


REM Zip the contents in it. Exclude all Thumbs.db files, and any folders beginning with "_".
REM EDIT: Hardcoded specific names (required scripts like '_yt-main.js' were being excluded causing major issues).
7z a -r !tempFilename! * m=Deflate -x^^!Thumbs.db -xr^^!_editable -xr^^!_editables -xr^^!_original -xr^^!_originals -xr^^!_defunct -xr^^!defunct

REM For each parameter from %3% onwards
set /a i=1
for %%A in (%*) do (

    if !i! gtr 3 (

        REM Split the filename string on "--", and get the first element (this removes all modifiers from the filename)
        FOR /F "TOKENS=1 DELIMS=--" %%G IN (%%A) DO (
            set fileNormal=%%G%%~xA
            set fileMod=%%A
            
            REM Remove the fileNormal file from the .zip (if it exists)
            7z d !tempFilename! !fileNormal!

            REM Rename the fileMod file to have the name of the fileNormal file.
            7z rn !tempFilename! !fileMod! !fileNormal!
        )
    )

    set /a i=!i!+1
)

REM Move and rename the .zip to the outputDir.
REM ECHO  "!cd!\!tempFilename!" %2% > cmd.log
move /Y "!cd!\!tempFilename!" %2%

REM If the errorlevel is NOT 0, then pause the cmd window (dont automatically close it). 
if %ERRORLEVEL% NEQ 0 (
    ECHO ErrorLevel is %ERRORLEVEL%
    ECHO.

    REM Create an error file in the destination instead.
    REM Navigate to the output directory
    cd /D %~p2
    ECHO %ERRORLEVEL% - Inspect the popup CMD window for details > %~n2.error.log

    pause
    EXIT /B %ERRORLEVEL%
)

EXIT /B 0
EndLocal
ECHO ON