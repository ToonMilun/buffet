@echo off
setlocal enableextensions enabledelayedexpansion

REM %1% dir

REM Important! Assign the parameters to normal variables (otherwise, unexpected bugs seem to occur).
set dir=%1%

ECHO dir:  !dir!
ECHO.

REM Navigate into it.
CD /D !dir!

REM Install node in the PARENT directory.
REM This will save on storage space, as all the topics in the unit's folder (should) use the same node_modules anyway.
npm install --prefix ../


REM Perform an update to browserlist. Automatically answer 'y' when prompted.
echo y | npx browserslist@latest --update-db


ECHO.

REM Pause if there's an error.
if %ERRORLEVEL% NEQ 0 (
    ECHO ErrorLevel is %ERRORLEVEL%
    ECHO.

    pause
    EXIT /B %ERRORLEVEL%
)

EXIT /B 0
EndLocal
ECHO ON