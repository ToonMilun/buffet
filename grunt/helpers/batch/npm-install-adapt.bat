@echo off
setlocal enableextensions enabledelayedexpansion

REM Run "npm install" on the provided directory.

REM %1%     = LOCAL Adapt directory to run "npm install" on.

REM Navigate to the directory.
cd /D %1%

REM Run the command in a new window
REM TODO: Need to allow these to be done asynchronously (at the moment, all Topics are done one-by-one).
call "npm install" cmd /c "npm install & pause & exit"

EXIT /B 0
EndLocal
ECHO ON