@echo off
setlocal enableextensions enabledelayedexpansion

REM %1% dir

REM Important! Assign the parameters to normal variables (otherwise, unexpected bugs seem to occur).
set dir=%1%

ECHO dir:  !dir!
ECHO.

REM Navigate into it.
CD /D !dir!

REM Pull
git pull --verbose

ECHO.

REM Pause if there's an error.
if %ERRORLEVEL% NEQ 0 (
    ECHO ErrorLevel is %ERRORLEVEL%
    ECHO.

    pause
    EXIT /B %ERRORLEVEL%
)

EXIT /B 0
EndLocal
ECHO ON