@echo off
setlocal enableextensions enabledelayedexpansion

REM Run "grunt build %*%" on the provided directory.

REM %1%     = LOCAL Adapt directory to run "grunt build" on.
REM %2%     = Output directory and final .zip filename (move the zip into here).
REM %3%+    = ALL --modifiers that will be used, wrapped in "". Example: "--mod1 --mod2". The "" will need to be escaped.

REM Navigate to the directory.
cd /D %1%

REM Join the mods parameters into one string
set mods=
set /a i=1
for %%A in (%*) do (
    REM From %3% onwards...
    if !i! gtr 2 (
        REM DO NOT REMOVE THE WHITESPACE CHARACTER AFTER THIS LINE \/
        set mods=!mods!%%A 
    )
    set /a i=!i!+1
)

set logfile=%~dp0..\..\..\output\_build-adapt.log

REM Run the command
REM ECHO ================================================================================ >> !logfile!
REM ECHO %1% >> !logfile!
REM ECHO grunt build %mods% >> !logfile!
REM ECHO ================================================================================ >> !logfile!
REM ECHO. >> !logfile!
REM Run the build task in a new window.

REM start "grunt build" cmd /c "'grunt build %mods% | tee !logfile!' & exit"
call grunt build-epub %mods%

REM The "grunt build" will set the %errorlevel% if it fails (thank goodness; it's a huge help that it does).
REM If the errorlevel is NOT 0, then pause the cmd window (dont automatically close it). 
if %ERRORLEVEL% NEQ 0 (
    ECHO ErrorLevel is %ERRORLEVEL%
    ECHO.
    
    REM Create an error file in the destination instead.
    REM Navigate to the output directory
    cd /D %~p2
    ECHO %ERRORLEVEL% - Inspect the popup CMD window for details > %~n2.error.log

    pause
    EXIT /B %ERRORLEVEL%
)

REM If the build succeeded without error, ZIP and move it's "build" directory to the output directory (%2%)
set tempFilename=__buffet-tmp.zip
cd /D build

REM Exclude all Thumbs.db files.
7z a -r !tempFilename! * m=Deflate -x^^!Thumbs.db

REM Move and rename the .zip to the outputDir.
REM ECHO  "!cd!\!tempFilename!" %2% > cmd.log
move /Y "!cd!\!tempFilename!" %2%

REM If the errorlevel is NOT 0, then pause the cmd window (dont automatically close it). 
if %ERRORLEVEL% NEQ 0 (
    ECHO ErrorLevel is %ERRORLEVEL%
    ECHO.

    REM Create an error file in the destination instead.
    REM Navigate to the output directory
    cd /D %~p2
    ECHO %ERRORLEVEL% - Inspect the popup CMD window for details > %~n2.error.log

    pause
    EXIT /B %ERRORLEVEL%
)

EXIT /B 0
EndLocal
ECHO ON