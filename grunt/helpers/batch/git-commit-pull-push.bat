@echo off
setlocal enableextensions enabledelayedexpansion

REM %1%         dir
REM %2%+        modifiers (all remaining parameters are modifiers)

REM Important! Assign the parameters to normal variables (otherwise, unexpected bugs seem to occur).
set dir=%1%
set mods=

REM Join the mods parameters into one string
set /a i=1
for %%A in (%*) do (
    REM From %2% onwards...
    if !i! gtr 1 (
        REM DO NOT REMOVE THE WHITESPACE CHARACTER AFTER THIS LINE \/
        set mods=!mods!%%A 
    )
    set /a i=!i!+1
)

ECHO dir: !dir!
ECHO mods: !mods!

ECHO.

REM Navigate into it.
CD /D !dir!

REM Commit all files.
git add .
git commit -m "<AUTO COMMIT BY BUFFET> !mods!"

REM Pull first
git pull origin master

REM then push all files.
git push origin master

ECHO.

REM Pause if there's an error.
if %ERRORLEVEL% NEQ 0 (
    ECHO ErrorLevel is %ERRORLEVEL%
    ECHO.

    pause
    EXIT /B %ERRORLEVEL%
)

EXIT /B 0
EndLocal
ECHO ON