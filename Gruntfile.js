module.exports = function(grunt){

  const path = require('path');
  const _ = require('underscore');
  const os = require("os");
  const homedir = os.homedir(); // Example: C:\users\mplotkin 
                                // WARNING!!! Will only work on Windows and Mac OS (though, you should only run these scripts on Windows anyway).
  const documentsDir = path.join(homedir, "Documents");

  grunt.loadNpmTasks('grunt-prompt');

  let configOverride = {};
  /*
  BUGGY. Do not use.
  try {
    configOverride = grunt.file.readJSON("config.json");
  }
  catch (err) {
    
  }*/

  require('load-grunt-config')(grunt, {
    configPath: path.join(process.cwd(), 'grunt/config'),
    jitGrunt: {
      customTasksDir: 'grunt/tasks'
    },
    data: _.extend(
      {}, 
      {
        buffetExt: "buffet",              // Buffet file extension name (*.buffet).
        tinsFile: 'files\\tins.buffet',   // accessible with '<%= tinsFile %>'
        tinLength: 5,                     // How long a TIN string should be.
        buffetDir: 'files\\units',        // accessible with '<%= buffetDir %>'
        outputDir: 'output',              // accessible with '<%= outputDir %>'
        batchDir: 'grunt\\helpers\\batch',
        extensionsDir: 'extensions',
        localRepoDir: path.join(documentsDir, "buffet-repos"),
        localAdaptRepoDir: path.join(documentsDir, "buffet-repos", "adapt"),
        savFile: 'main.sav'
      },
      configOverride // Allow for custom overrides in config.json.
    )
  });
  
};